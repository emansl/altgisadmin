Alternate GeoDjango Admin
===========================

This package provides a OSMGeoAdmin (from GeoDjango) subclass and
changes the default settings so they don't need to be repeately set
in applications which provide a GeoDjango admin interface.

- Provides a locally hosted version 2.13 OpenLayers.js file
- Sets `openlayers_url` to `static/admin/js/OpenLayers.js`
- TODO: displays Satellite Photos instead of Open Street Map tiles
