__version__ = '0.4.0'
__license__ = 'BSD 3-Clause'
__copyright__ = 'Copyright 2016 Kevala Analytics, Inc.'

VERSION = __version__
