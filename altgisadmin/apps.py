from django.apps import AppConfig


class AltgisConfig(AppConfig):
    name = 'altgisadmin'
