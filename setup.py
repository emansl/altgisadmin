#!/usr/bin/env python
"""
Alternate GeoDjango Admin
===========================

:copyright: (c) 2016 by Kevala Analytics, Inc.
:license: BSD
"""
from setuptools import setup, find_packages


def readme():
    with open('README.md') as f:
        return f.read()

install_requires = [
    'Django',
]

setup(name='altgisadmin',
      version='0.4.0',
      description="Alternate GeoDjango Admin",
      long_description=readme(),
      author='Kevala Analytics, Inc.',
      author_email='eman@kevalaanalytics.com',
      url='https://gitlab.com/emansl/altgisadmin',
      install_requires=install_requires,
      test_suite='nose.collector',
      tests_require=["nose", "nose-cover3"],
      license="BSD",
      zip_safe=False,
      classifiers=[
          'Development Status :: 4 - Beta',
          'Intended Audience :: Developers',
          'Topic :: GeoDjango',
          'Operating System :: OS Independent',
          'Programming Language :: Python :: 3.5',
      ],
      keywords='GeoDjango Admin',
      packages=find_packages(),
      include_package_data=True,
      package_data={'altgisadmin': ['static/openlayers/*.js',
                                    'static/openlayers/img/*',
                                    'static/openlayers/theme/default/*.css',
                                    'static/openlayers/theme/default/img/*']}
      )
